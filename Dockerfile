#
# BUILD NOTES


# 
# STAGE 1 - build unrar and par2cmdline
FROM alpine:3.18 as prepstage


# prepare unrar
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
WORKDIR /build
RUN \
  apk add --no-cache curl build-base g++ gcc libffi-dev make openssl-dev python3-dev && \
  curl -L -o /build/unrar.tar.gz -L "https://www.rarlab.com/rar/unrarsrc-6.2.12.tar.gz" && \  
  tar xf /build/unrar.tar.gz -C /build --strip-components=1 && \
  sed -i 's|LDFLAGS=-pthread|LDFLAGS=-pthread -static|' makefile && \
  make && \
  chmod a+x unrar 

# prepare par2cmdline-turbo
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
WORKDIR /build/par2
RUN \
  apk add --no-cache git automake make autoconf && \
  git clone https://github.com/animetosho/par2cmdline-turbo.git /build/par2 && \
  aclocal && \
  automake --add-missing && \
  autoconf && \
  ./configure && \
  make && \
  make install 

# 
# STAGE 2 - build sabnzbd
FROM alpine:3.18

# copy utilitilities from prep stage
COPY --from=prepstage /build/unrar /usr/local/bin/
COPY --from=prepstage /usr/local/bin/par2 /usr/local/bin

SHELL ["/bin/ash", "-o", "pipefail", "-c"]
WORKDIR /app/nzbnotify
RUN \
  apk add -U --update --no-cache tzdata curl jq 7zip python3 py3-setuptools && \
  apk add -U --update --no-cache --virtual=build-dependencies build-base python3-dev py-pip libffi-dev openssl-dev autoconf automake && \
  NZBNOTIFY_VERSION=$(curl -s "https://api.github.com/repos/caronc/nzb-notify/releases/latest" | jq -r .tag_name ) && \
  curl -o /tmp/nzbnotify.tar.gz -L "https://api.github.com/repos/caronc/nzb-notify/tarball/${NZBNOTIFY_VERSION}" && \
  tar xf /tmp/nzbnotify.tar.gz -C /app/nzbnotify --strip-components=1 && \
  pip install --no-cache-dir -r requirements.txt && \
  apk del --purge build-dependencies && \
  rm -rf /tmp/* /var/tmp/* "$HOME/.cache" /var/cache/apk/*

SHELL ["/bin/ash", "-o", "pipefail", "-c"]
WORKDIR /app/sabnzbd
RUN \
  apk add -U --update --no-cache --virtual=build-dependencies build-base python3-dev py-pip libffi-dev openssl-dev autoconf automake && \
  SABNZBD_VERSION=$(curl -s "https://api.github.com/repos/sabnzbd/sabnzbd/releases/latest" | jq -r .tag_name) && \
  export SABNZBD_VERSION && \
  curl -o /tmp/sabnzbd.tar.gz -L "https://github.com/sabnzbd/sabnzbd/releases/download/${SABNZBD_VERSION}/SABnzbd-${SABNZBD_VERSION}-src.tar.gz" && \
  tar xf /tmp/sabnzbd.tar.gz -C /app/sabnzbd --strip-components=1 && \
  pip install --no-cache-dir -U wheel apprise pynzb requests && \
  # remove cheetah from requirements
  #grep -v "cheetah3" requirements.txt > requirements.tmp && mv requirements.tmp requirements.txt && \
  pip install -U --no-cache-dir -r requirements.txt && \
  apk del --purge build-dependencies && \
  rm -rf /tmp/* /var/tmp/* "$HOME/.cache" /var/cache/apk/*

RUN \
  mkdir /config /complete /incomplete

## uncomment to start with minimal config file
#COPY /templates/testing.ini /config/sabnzbd.ini

EXPOSE 8123
VOLUME ["/config", "/complete", "/incomplete"]

HEALTHCHECK --interval=1m --timeout=5s --start-period=20s \
    CMD curl -fsSL "http://localhost:8123/" || exit 1

ENTRYPOINT [ "/usr/bin/python3", "/app/sabnzbd/SABnzbd.py", "-f", "/config/sabnzbd.ini", "-b", "0", "-s", "0.0.0.0:8123" ]

# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-sabnzbd"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="Sabnzbd"
LABEL org.label-schema.schema-version="$SABNZBD_VERSION"

