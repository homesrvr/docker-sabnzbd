# docker-sabnzbd

[![pipeline status](https://gitlab.com/homesrvr/docker-sabnzbd/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-sabnzbd/commits/main) 
[![Sabnzbd Release](https://gitlab.com/homesrvr/docker-sabnzbd/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-sabnzbd/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker link](https://gitlab.com/homesrvr/docker-sabnzbd/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/sabnzbd)
[![](https://img.shields.io/docker/image-size/culater/sabnzbd/latest)](https://hub.docker.com/r/culater/sabnzbd)
[![](https://img.shields.io/docker/pulls/culater/sabnzbd?color=%23099cec)](https://hub.docker.com/r/culater/sabnzbd)


This is an alpine-based dockerized build of **sabnzbd**.
This is part of a collection of docker images, designed to run on my low-end x86 based QNAP NAS server. My aim is to create an image with a small footprint. 

**Default Port**: Please note that this image uses port 8123 by default, not 8080 as most others do. Port 8080 is used by a lot of tools and was problematic within my environment.
You can still use 8080 by mapping the port on the command line or in the docker-compose, e.g. like this:
> -p 8080:8123

# Docker
The resulting docker image can be found here [https://hub.docker.com/r/culater/sabnzbd](https://hub.docker.com/r/culater/sabnzbd)

# Notifications
This image has nzbnotify asll well as apprise built in for notifications. <p>
Nzbnotify can be setup as follows:
1. Goto _Config_ > _Folders_ > _Scripts Folder_ and set **/app/nzbnotify** as the script folder, then click <Save Changes>
1. Goto _Config_ > _Notifications_ and select **Enable Notification Script**
1. Set the _Script_ dropdown to **sabnzbd-notify.py**
1. Add your individual notification parameters to the **Parameters** input field, then <Save Changes>

Check the [sabnzb wiki](https://sabnzbd.org/wiki/configuration/4.1/scripts/notification-scripts) for more information on using notifications.

# Example usage

docker run example:
```yaml
docker run -d \
  -v [/configdir]:/config \
  -v [/completedir]:/complete \
  -v [/incompletedir]:/incomplete \
  -p 8123:8123 \
  culater/sabnzbd
````

docker-compose example:
```yaml
---
version: "2.1"
services:
  sabnzbd:
    image: culater/sabnzbd
    container_name: sabnzbd
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Berlin
    volumes:
      - /path/to/data:/config
      - /path/to/downloads:/downloads #optional
      - /path/to/incomplete/downloads:/incomplete-downloads #optional
    ports:
      - 8123:8123
    restart: unless-stopped
````

## Reporting problems
Please report any issues to the [Gitlab issue tracker](https://gitlab.com/homesrvr/docker-sabnzbd/-/issues)

## Authors and acknowledgment
More information about sabnzbd can be found here:
[SABnzbd](https://sabnzbd.org "SABnzbd Project Homepage") 


## Project status
The docker image auto-updates after a new release of sabnzbd within a few days. The current sabnzbd version can be seen from the release tag. 

